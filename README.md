# compiler-bootstrap

This is a repository with binaries of the [Clean][] [compiler][]. It is used to
bootstrap the build of the [compiler][] itself.

This package should not be used directly. Instead, use `base-compiler` (which
is included in `base`).

## Maintainer & license

This project is maintained by [Camil Staps][].

For licensing details, see the repository of the [compiler][].

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[compiler]: https://gitlab.com/clean-and-itasks/base/compiler
